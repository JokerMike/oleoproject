<?php

namespace App\Http\Controllers;

use App\Traccar;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;



class ConnexionController extends Controller
{
    public function connexion(Request $request)
    {

        $client = new Client();
        $traccar = new Traccar();
        try {
            $response = $client->request('POST', $traccar->getBase_uri()."/session", [
                'form_params' => [
                    'email' => $request["username"],
                    'password' =>$request["password"],
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $req = $e->getRequest();
            $resp =$e->getResponse()->getStatusCode();
            if ($resp == 401) {
                Alert::error('Erreur', 'Nom d\'utilisateur ou mot de passe incorrect');
                return redirect()->back();
            }

        }

        if ($response->getStatusCode() == 200) {


            $session = json_decode($response->getBody());

            $response = $client->request('GET', $traccar->getBase_uri()."/session", [
                'query' =>[
                    'token' =>"rYfboPEpLvZntaaEjgSiX353Vc7WvK7x",
                ],

            ]);

           return redirect()->away($traccar->get_redirect());

        }

    }
}
