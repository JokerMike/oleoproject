
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
    <meta name="author" content="OLEO"/>
    <meta name="keywords" content="oleo, tracking, gps"/>
    <title>OLEO GPS</title>


    <link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/plugins/fontawesome/css/all.min.css" rel="stylesheet"/>

    <link href="/css/general.css" rel="stylesheet"/>
    <link href="/css/styles.css" rel="stylesheet"/>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-8 col-lg-8 col-md-7 col-sm-6 col-12 h100vh" style="background: linear-gradient(black, red)">
            <div class="col-12 text-white text-center m-t-50">
                <h1>OLEO</h1>
                <h2>Numero 1 du tracking du Togo</h2>
            </div>
            <div class="col-12 text-white m-t-50">
                <h3><img src="/images/login-gps.png" alt="Tracking par GPS" width="50"> Tracking par GPS</h3>
                <h4>Suivez en temps réel le déplacement de vos engins lourds, camions, voitures, motos, marchandises, ...</h4>

            </div>
            <div class="col-12 text-white m-t-30 text">
                <h3><img src="/images/login-repair.png" alt="Maintenance Prédictive" width="50"> Maintenance Prédictive</h3>
                <h4>Le Système vous informera en temps réel sur les pièces à rechanger, les maintenances à effectuer avec des statistiques poussées ...</h4>
            </div>
        </div>

        <div class="col-xl-4 col-lg-4 col-md-5 col-sm-6 col-12 align-items-center justify-content-center">

            <div class="text-center">

                @include('sweetalert::alert')

            </div>

            <form action="{{route('connexion')}}" method="post">
                @csrf

                <div class="col-12 mt-3 mt-sm-5 text-center">
                    <img src="/images/oleo.jpg" width="100%"  alt="oleo"/>
                </div>

                <div class="col-12 mt-3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-user text-primary"></i>
                            </span>
                        </div>
                        <input name="username" type="text" id="username" placeholder="nom d'utilisateur" class="form-control" autofocus="autofocus"/>
                    </div>
                </div>

                <div class="col-12 mt-3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="fas fa-key text-primary"></i>
                        </span>
                        </div>
                        <input name="password" id="password" placeholder="mot de passe" type="password" class="form-control" />
                    </div>
                </div>

                <div class="col-12 mt-3 mb-4">

                    <button class="btn btn-sm btn-primary" type="submit"> <i class="fa fa-sign-in-alt fa-lg"></i> Se Connecter </button>
                </div>



            </form>

        </div>
    </div>


</div>

</body>

<script src="/plugins/jquery/jquery.min.js"></script>
<script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/scripts/login.js"></script>

</html>
